package solorzanomedina.facci.practica4_4b;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener
{
    Button FragUno, FragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragDos = findViewById(R.id.btnFragDos);
        FragUno = findViewById(R.id.btnFragUno);
        FragUno.setOnClickListener(this);
        FragDos.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFragUno:
                FragUno fragUno = new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragUno);
                transactionUno.commit();
                break;
            case R.id.btnFragDos:
                FragDos fragDos = new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragDos);
                transactionDos.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
